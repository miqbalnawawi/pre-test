#!/bin/bash

# Menyimpan output log ke dalam variabel
logs=$(docker logs --since="10m" nginx-error-app)

# Mencetak log yang mengandung "500"
echo "$logs" | grep "500"
